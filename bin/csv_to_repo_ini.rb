#!/usr/bin/ruby

require 'csv'

CSV
  .read(ARGV.first, headers: true)
  .sort_by { |row| "#{File.basename(row['github'].downcase)}_github"}
  .each do |row|
    puts <<~EOS
      [#{File.basename(row['github'].downcase)}_github]
        title = Github:#{row['name']}
        link  = #{row['github']}
        feed  = #{row['github']}.atom

    EOS
end
