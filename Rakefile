require 'fileutils'
require 'pathname'

db_pathname     = Pathname('./.db')
public_pathname = Pathname('./public')
verbose_flag    = ENV['VERBOSE'] ? '--verbose' : '--quieter'

namespace :build do
  desc 'Build the index and other pages'
  task :pages do
    public_pathname.mkpath

    puts 'Build index page'
    system("haml --trace source/index.html.haml #{public_pathname.join('index.html')}")
    system("haml --trace source/map.html.haml #{public_pathname.join('map.html')}")
  end

  desc 'Build the feed pages'
  task :feeds do
    db_pathname.mkpath
    public_pathname.mkpath

    puts 'Build feeds'
    system(
      "pluto #{verbose_flag} update --dbpath=#{db_pathname} --dbname='pluto.db' ./data/*.ini"
    )
    system(
      "pluto #{verbose_flag} merge --template=wrdashboard --dbpath=#{db_pathname} --dbname='pluto.db' --output=#{public_pathname} ./data/*.ini"
    )
  end

  task all: %i[pages feeds]
end

desc 'Generate the feed pages without fetching'
task :merge do
  public_pathname.mkpath

  puts 'Build feeds'
  system(
    "pluto #{verbose_flag} build --template=wrdashboard --dbpath=#{db_pathname} --dbname='pluto.db' --output=#{public_pathname} ./data/*.ini"
  )
end

desc 'Server the files build into the public directory'
task :server do
  require 'webrick'

  server = WEBrick::HTTPServer.new(
    Port:         4587,
    DocumentRoot: public_pathname.expand_path
  )
  puts "View the site at http://localhost:4587"
  trap('INT') { server.shutdown }
  server.start
end

task default: 'build:all'
